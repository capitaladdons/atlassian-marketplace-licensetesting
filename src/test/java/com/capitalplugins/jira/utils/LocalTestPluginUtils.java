package com.capitalplugins.jira.utils;

import java.io.File;
import java.io.IOException;

/**
 * PluginTestUtils
 * Purpose:
 * <p/>
 * Shameless plug: Capital City Consultants https://www.capitalcityconsultants.com / Capital Plugins https://capitalplugins.atlassian.net
 * Author: wac
 * Created: 11/25/13 - 4:54 AM
 */
public interface LocalTestPluginUtils
{
    /** Copies the test plugins to a new temporary directory and returns that directory. */
    File copyTestPluginsToTempDirectory() throws IOException;

    /** Returns the version string (if there is one) of the plugin from the Plugin Manifest */
    String getPluginVersion( Class<?> aClass );

    /** Returns the directory on the classpath where the test plugins live. */
    File getTestPluginsDirectory();

    File getPluginFile( String filename );

    void clean();

    void uploadPluginViaUPM( File pluginJar ) throws Exception;
}
