package com.capitalplugins.jira.utils;

import com.atlassian.plugin.util.ClassLoaderUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 *  Imports that aren't needed since I gutted the functionality
 */
/*
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;

import java.net.URI;

*/

/**
 * PluginTestUtils
 * Purpose:
 * <p/>
 * Shameless plug: Capital City Consultants https://www.capitalcityconsultants.com / Capital Plugins https://capitalplugins.atlassian.net
 * Author: wac
 * Created: 11/25/13 - 12:21 AM
 */
public class LocalTestPluginUtilsImpl implements LocalTestPluginUtils
{

    @Override
    public File copyTestPluginsToTempDirectory() throws IOException
    {
        File directory = createTemporaryDirectory();
        FileUtils.copyDirectory(getTestPluginsDirectory(), directory);

        // Clean up version control files in case we copied them by mistake.
        FileUtils.deleteDirectory(new File(directory, "CVS"));
        FileUtils.deleteDirectory(new File(directory, ".svn"));

        return directory;
    }

    @Override
    public String getPluginVersion( Class<?> aClass )
    {
        // Set the plugin version to display
        String result = aClass.getClass().getPackage().getImplementationVersion();
        if ("".equals(result) || result == null)
        {
            result = "version (Unknown)";
        }

        if (result.endsWith("build )"))
        {
            result = "version (Debugging Build)";
        }
        return result;
    }

    @Override
    public File getTestPluginsDirectory()
    {
        URL url = ClassLoaderUtils.getResource(TestConstants.TEST_PLUGIN_DIRECTORY, LocalTestPluginUtilsImpl.class);
        return new File(url.getFile());
    }

    @Override
    public File getPluginFile( String filename )
    {
        return new File(getTestPluginsDirectory(), filename);
    }

    private File createTemporaryDirectory()
    {
        File tempDir = new File(TestConstants.PLUGINS_TEMP_DIRECTORY, "plugins-" + randomString(6));
        FileUtils.deleteQuietly(tempDir);
        tempDir.mkdirs();
        return tempDir;
    }

    @Override
    public void clean()
    {
        FileUtils.deleteQuietly(TestConstants.PLUGINS_TEMP_DIRECTORY);
    }

    /**
     * Generate a random string of characters - including numbers
     *
     * @param length the length of the string to return
     *
     * @return a random string of characters
     */
    private String randomString( int length )
    {
        StringBuffer b = new StringBuffer(length);

        for (int i = 0; i < length; i++)
        {
            b.append(randomAlpha());
        }

        return b.toString();
    }

    /**
     * Generate a random character from the alphabet - either a-z or A-Z
     *
     * @return a random alphabetic character
     */
    private char randomAlpha()
    {
        int i = ( int ) (Math.random() * 52);

        if (i > 25)
        {
            return ( char ) (97 + i - 26);
        }
        else
        {
            return ( char ) (65 + i);
        }
    }

    @Override
    public void uploadPluginViaUPM( File pluginJar ) throws Exception
    {
        throw new Exception("Routine Not supported Yet");

    }
}
