package com.capitalplugins.jira.utils;

/**
 * TestConstants
 * Purpose:
 * <p/>
 * Shameless plug: Capital City Consultants https://www.capitalcityconsultants.com / Capital Plugins https://capitalplugins.atlassian.net
 * Author: wac
 * Created: 11/25/13 - 12:26 AM
 */

import java.io.File;

public final class TestConstants
{
    public static final String JWSP_GROUPID         = "com.capitalplugins.jira";
    public static final String JWSP_TEST_PLUGIN_KEY = JWSP_GROUPID + ".example-license-testing-tests";

    public static final String TEXT_FIELD_CF_PLUGIN_KEY = "com.atlassian.jira.plugin.system.customfieldtypes";

    public static final String JUNK_CLASS = "i.do.not.exist.Junk";

    public static final File   PLUGINS_TEMP_DIRECTORY = new File("target/plugins-temp");
    public static final String TEST_PLUGIN_DIRECTORY  = "test-plugins";

    public static final String UPM_ROOT_RESOURCE = "/rest/plugins/1.0/";

    public static final String CLEAN_FILENAME_PATTERN = "[:\\\\/*?|<> _]";

    public static final String CUSTOMFIELD_ID_TOKEN = "%CUSTOMFIELDID%";
    public static final String SCREEN_ID_TOKEN      = "%SCREENID%";

}
