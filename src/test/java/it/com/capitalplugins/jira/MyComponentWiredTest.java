package it.com.capitalplugins.jira;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.ApplicationProperties;
import com.capitalplugins.jira.MyPluginComponent;
import com.capitalplugins.jira.utils.LocalTestPluginUtils;
import com.capitalplugins.jira.utils.LocalTestPluginUtilsImpl;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
//import com.atlassian.maven.plugins.amps.Product;

@RunWith( AtlassianPluginsTestRunner.class )
public class MyComponentWiredTest
{
    Logger log = Logger.getLogger(MyComponentWiredTest.class);

    private final ApplicationProperties applicationProperties;
    private final MyPluginComponent     myPluginComponent;

    private final PluginAccessor       pluginAccessor;
    private final LocalTestPluginUtils localTestUtils;

    public MyComponentWiredTest( ApplicationProperties applicationProperties, MyPluginComponent myPluginComponent, PluginAccessor pluginAccessor )
    {
        this.applicationProperties = applicationProperties;
        this.myPluginComponent = myPluginComponent;
        this.pluginAccessor = pluginAccessor;
        this.localTestUtils = new LocalTestPluginUtilsImpl();
    }

    @Test
    public void testMyName()
    {
        assertEquals("names do not match!", "myComponent:" + applicationProperties.getDisplayName(), myPluginComponent.getName());
    }

    @Test
    public void testMyPluginInformation()
    {
        String intel = myPluginComponent.getMyPluginInformation();

        log.debug("Here is the plugin intel: \n" + intel);

        assertNotNull("Testing if we are able to access myPlugin's getInformation type method", intel);
    }

}
