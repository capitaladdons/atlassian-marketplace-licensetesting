package com.capitalplugins.jira;

public interface MyPluginComponent
{
    String getName();

    int addNumbers( int x, int y );

    String getMyPluginInformation();
    String getMyPluginVersion();
}
