package com.capitalplugins.jira;

import com.atlassian.sal.api.ApplicationProperties;

import java.io.File;
import java.util.*;

public class MyPluginComponentImpl implements MyPluginComponent
{
    private final ApplicationProperties applicationProperties;

    public MyPluginComponentImpl( ApplicationProperties applicationProperties )
    {
        this.applicationProperties = applicationProperties;
    }

    public String getName()
    {
        if (null != applicationProperties)
        {
            return "myComponent:" + applicationProperties.getDisplayName();
        }

        return "myComponent";
    }

    public int addNumbers( int x, int y )
    {
        return x + y;
    }

    public String getMyPluginInformation()
    {
        String result = null;
        if (applicationProperties != null)
        {
            String displayName = applicationProperties.getDisplayName();
            String buildNumber = applicationProperties.getBuildNumber();
            String baseURL = applicationProperties.getBaseUrl();
            Date buildDate = applicationProperties.getBuildDate();
            File homeDir = applicationProperties.getHomeDirectory();
            String version = applicationProperties.getVersion();
            String atlasInfo = applicationProperties.toString();

            result = "Capital Plugins Testing Example Plugin Information: \n";
            result += "     DisplayName: " + displayName + "\n";
            result += "       BuildDate: " + buildDate.toString() + "\n";
            result += "         Version: " + version + "\n";
            result += "  CCC LC Version: " + getMyPluginVersion() + "\n";
            result += "    Build Number: " + buildNumber + "\n";
            result += "  Home Directory: " + homeDir + "\n";
            result += "\n";
            result += "Atlassian's Version: " + atlasInfo + "\n";
            result += "Base URL: " + baseURL + "\n";

        }
        System.out.println(result);
        return result;
    }

    public String getMyPluginVersion()
    {
        // Set the plugin version to display
        String result = this.getClass().getPackage().getImplementationVersion();
        if ("".equals(result) || result == null)
        {
            result = "UnTagged Debug Version";
        }

        if (result.endsWith("build )"))
        {
            result = "version (Debugging Build)";
        }
        return result;
    }

}
