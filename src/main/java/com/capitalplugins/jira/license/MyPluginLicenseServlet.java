package com.capitalplugins.jira.license;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import java.io.Serializable;

/**
 * MyPluginLicenseServlet
 * Purpose:
 * <p/>
 * Shameless plug: Capital City Consultants https://www.capitalcityconsultants.com / Capital Plugins https://capitalplugins.atlassian.net
 * Author: wac
 * Created: 11/29/13 - 9:02 PM
 */
public interface MyPluginLicenseServlet extends Servlet, ServletConfig, Serializable
{
}
